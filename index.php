<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable: no-scalable">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Fizztest</title>

    <!-- seo metatags and favicon -->
    <?php include 'includes/metatags.php' ?>

    <!-- libs -->
    <link rel="stylesheet" href="node_modules/swiper/dist/css/swiper.min.css">
    <link rel="stylesheet" href="node_modules/normalize.css/normalize.css">

    <!-- custom style -->
    <link rel="stylesheet" href="styles/main.min.css">
    <link rel="stylesheet" href="styles/icon.min.css">
    <link rel="stylesheet" href="styles/mobile.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->
  </head>

  <body>

    <?php
      include 'includes/header.php'
    ?>

    <section class="carousel">
      <div class="container">
        <div class="swiper-container">
          <div class="swiper-wrapper">
            <div class="swiper-slide">
              <img src="images/racing-980x400.jpg" alt="Racing club">
            </div>
            <div class="swiper-slide">
              <img src="images/racing-980x400.jpg" alt="Fizztest tienda">
            </div>
            <div class="swiper-slide">
              <img src="images/racing-980x400.jpg" alt="Venta indumentaria">
            </div>
          </div>
          <!-- Add Pagination -->
          <div class="swiper-pagination"></div>
        </div>
      </div>
    </section>

    <section class="company">
      <div class="container">
        <img src="images/marcas.jpg" alt="">
      </div>
    </section>


    <section class="catalogue">
      <div class="container">
        <div class="filter">

          <p>Filtro por</p>

          <div class="options gender">
            <p>Sexo</p><i class="icon icon-arrow-top">
            </i>
          </div>
          <div class="panel-options gender">
            <ul>
              <li>
                <label>
                  <input type="checkbox" name="gender" value="m">
                  Hombre
                </label>
              </li>
              <li>
                <label>
                  <input type="checkbox" name="gender" value="f">
                  Mujer
                </label>
              </li>
              <li>
                <label>
                  <input type="checkbox" name="gender" value="b">
                  Niños
                </label>
              </li>
            </ul>
          </div>

          <div class="options sport">
            <p>Deporte</p><i class="icon icon-arrow-top">
            </i>
          </div>
          <div class="panel-options sport">
            <ul>
              <li>
                <label>
                  <input type="checkbox" name="sport" value="t">
                  Tenis
                </label>
              </li>
              <li>
                <label>
                  <input type="checkbox" name="sport" value="r">
                  Rugby
                </label>
              </li>
              <li>
                <label>
                  <input type="checkbox" name="sport" value="f">
                  Fútbol
                </label>
              </li>
            </ul>
          </div>

          <div class="options colors">
            <p>Color</p><i class="icon icon-arrow-top">
            </i>
          </div>
          <div class="panel-options colors">
            <ul>
              <li>
                <label>
                  <input type="checkbox" name="colors" value="t">
                  Negro
                </label>
              </li>
              <li>
                <label>
                  <input type="checkbox" name="colors" value="a">
                  Azul
                </label>
              </li>
              <li>
                <label>
                  <input type="checkbox" name="colors" value="v">
                  Violeta
                </label>
              </li>
            </ul>
          </div>

        </div>

        <section class="gallery">
        </div>

      </div>
    </section>




    <?php
      include 'includes/footer.php'
    ?>

    <!-- libs -->
    <script src="node_modules/jquery/dist/jquery.min.js" type="text/javascript">
    </script>
    <script src="node_modules/swiper/dist/js/swiper.min.js" type="text/javascript"></script>

    <!-- custom script -->
    <script src="scripts/main.js" type="text/javascript"></script>

  </body>
</html>
