
/**
*	1) Añadir un evento a la cabezera de los filtros que al hacer click en ella se desplieguen los filtros si están ocultos, y los oculte si no lo están.
*
*/

/**
*
*	2) En el listado de productos, agregue la clase 'oferta' a todos los productos impares
*/

/**
*
*	3) Teniendo la variable 'products' agregue al listado los productos cuando el document esté listo
*
*/
var products = [
	{ title: 'Botín', price: 800.44, category: 'Botines', image: 'http://placehold.it/225x225', brand: 'nike' },
	{ title: 'Ojotas', price: 300.99, category: 'Ojotas', image: 'http://placehold.it/225x225', brand: 'adidas' },
	{ title: 'Zapatillas', price: 1120.00, category: 'Calzado', image: 'http://placehold.it/225x225', brand: 'puma' },
	{ title: 'Short', price: 320.44, category: 'Vestimenta', image: 'http://placehold.it/225x225', brand: 'nike' },
	{ title: 'Pantalón', price: 360.44, category: 'Natación', image: 'http://placehold.it/225x225', brand: 'nike' }	
];

/**
*	4) En el hover de la imagen del producto que cambié el color del precio a verde, y que aumente el tamaño del título en 2 píxeles.
*
*/

/**
*  5) Al hacer click sobre que aparecen debajo del slider principal ejecuta una llamada AJAX a la siguiente 
URL: http://162.243.104.98/ajax.php y si la llamada es satisfactoria ocultá las marcas, y mostrá el resultado devuelto dónde estaban las marcas.
*
*/
